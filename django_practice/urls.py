from django.urls import path
from . import views
urlpatterns = [
	path('', views.index, name='index'),
	path('<int:groceryitem_id>/', views.groceryitem, name='viewgroceryitem'),

	# /grocerylist/register
	path('register', views.register, name="register"),

	# /grocerylist/change_password
	path('change_password', views.change_password, name='change_password'),

	#/grocerylist/login
	path('login', views.login_view, name='login'),

	#/grocerylist/logout
	path('logout', views.logout_view, name='logout')
]